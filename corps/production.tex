\chapter{Notes de production}
\label{chap-production}


\begin{introchap}{Nous avons rassemblé} ici des éléments permettant
  d'exploiter les sources de ce document, comment les compiler, avec
  quelle distribution de \LaTeX{}, comment les fichiers sont
  organisés, etc.
\end{introchap}



\section{Prérequis}

Le présent ouvrage a été compilé sur GNU/Linux Ubuntu
16.04 et 18.04 avec les versions TEXlive 2015 et 2017, respectivement. Il ne devrait pas y avoir de problème particulier pour le
compiler avec d'autres distribution. Les prérequis sont donc :
\begin{itemize}
\item Une distribution \TeX, les paquets debian suivants devraient suffire :
\begin{verbatim}
texlive-base             texlive-fonts-recommended
texlive-binaries         texlive-lang-french         
texlive-extra-utils      texlive-latex-base          
texlive-font-utils       texlive-latex-extra         
texlive-fonts-extra      texlive-pictures
texlive-science          texlive-latex-recommended 
texlive-generic-recommended
\end{verbatim}  
\item  \soft{graph} de la suite \soft{plotutils}
\item  \soft{transfig} (fait partie de xfig)
\item  \soft{inkscape}
\item  \soft{gs} (ghostscript)
\item  \soft{ps2epsi} (ghostscript)
\item \soft{epstopdf} (doit faire partie de la distribution \TeX,
  sinon allez faire un saut sur la page https://ctan.org/pkg/epstopdf)
\item  \soft{pdfnup} (idem, https://ctan.org/pkg/pdfjam)
\end{itemize}

\section{Les sources du manuel}
\newcommand{\dossier}[1]{\texttt{#1}}

Les sources sont disponibles sur le GitLab de l'association Framasoft :
\newcommand{\arobase}{@}
\begin{unixcom}
  git clone git@arobase()framagit.org:framabook/onlysql.git
\end{unixcom}
Et :
\begin{unixcom}
  git pull 
\end{unixcom}
pour les mettre à jour une fois que vous aurez une copie en local chez vous.

\pagebreak[4]
\subsection{Structure}

Les sources du manuel sont organisées selon le principe suivant :
\begin{itemize}
\item document maître dans le fichier \texttt{framabook.tex}
  
\item sources \LaTeX{} des chapitres dans le répertoire
  \dossier{corps} avec un fichier par chapitre ;
\item les styles (\texttt{sty} et \texttt{cls}) dans le répertoire
  \dossier{styles} ;
\item les images dans un répertoire \dossier{pngs} ;
\item les sources \soft{xfig} et \soft{inkscape} dans le répertoire
  \dossier{figs} ;
\item tout ce qui a trait à l'index, à la bibliographie et au
  glossaire est stocké dans le répertoire \dossier{bibidx} ;
\end{itemize}
Les sources des dessins (\texttt{.fig} et \texttt{.svg}) sont traduits
au format pdf dans le répertoire \dossier{pdfs}.

\subsection{Styles}

Le fichier \texttt{framabook.cls} contient la définition de la classe
du manuel. Ce fichier fait appel à une série de packages «~du
commerce~» et une série de packages «~maison~». On trouve, pour ces
derniers, un fichier source pour :
\begin{itemize}
\item boîte avec un titre (\texttt{titlebox.sty}), onglets, nota,
  sommaire, glossaire, renvois (\texttt{voir.sty}), code source,
  commandes unix, lettrine, citations et épigraphes ;
\item le sommaire ;
\item la géométrie globale du document ;
\item l'allure des en-têtes et pieds de pages ;
\item l'allure des sections/chapitres/etc. ;
\item des commandes en vrac utilisées dans le document
  (dans le fichier \texttt{manumac.sty}) ;
\end{itemize}
Sauf indication contraire, ces fichiers portent un nom ressemblant
étrangement à ce qu'ils contiennent.


\section{Compilation}

\begin{enumerate}
\item Choisir la version à compiler, pour cela dans le document maître
  \texttt{framabook.tex}, choisir l'une des deux options de documents
  :
\begin{itemize}
\item soit \texttt{versionenligne} qui produit un pdf uniquement
  destiné à être lu
\item soit \texttt{versionpapier} pour un pdf destiné à être imprimé
\end{itemize}
\item Choisir la version couleur ou passer en noir \& blanc, dans ce
  cas ajouter \texttt{nb} dans les options de classe
\item  Créer les figures :
\begin{itemize}
\item  soit  :
  \begin{unixcom}
    make mode=nb figs
  \end{unixcom}
\item  soit :
  \begin{unixcom}
    make figs
  \end{unixcom}
\end{itemize}
\item \LaTeX{} Passe 1 :
  \begin{unixcom}
    pdflatex framabook
  \end{unixcom}

\item Création de la biblio et de l'index
\begin{unixcom}
  make bib
  make index
\end{unixcom}
\item \LaTeX{} passe 2 : \texttt{pdflatex framabook}
\item \LaTeX{} passe 3 : \texttt{pdflatex framabook}
\end{enumerate}
À l'issue de cette séquence de commandes un fichier
\texttt{framabook.pdf} doit contenir le manuel.

\begin{note}
  Si nécessaire, un script bash peut produire les trois versions d'un
  coup de cuillère à pot. Pour cela, il faudra tapoter :
  \begin{unixcom}
    ./makedistrib.sh
  \end{unixcom}
  \continuenota%
  Les trois fichiers produits seront nommés :
  \begin{enumerate}
  \item \texttt{onlysql-nb.pdf} version noir\&blanc à imprimer ;
  \item \texttt{onlysql-couleur.pdf} version couleur à imprimer ;
  \item \texttt{onlysql-enligne.pdf} version à destination d'un lecteur PDF.
  \end{enumerate}
\end{note}

\section{Imprimer}

Pour fabriquer le fichier \texttt{book.pdf} qui pourra être imprimé
sur du A4 avec une reliure longue et 2 pages A5 par page :
\begin{unixcom}
  make book
\end{unixcom}
Cette commande exploite le fichier \texttt{framabook.pdf} de la
section précédente. Si vous vouliez transformer l'un des fichiers
produit par le script \texttt{makedistrib.sh}, il faudra vous fendre
d'un :
\begin{unixcom}
  make MAITRE=onlysql-couleur book
\end{unixcom}

\section{Nettoyage}

Pour détruire toutes les figures générées en pdf à partir des sources xfig/svg :
\begin{unixcom}
  make cleanfigs
\end{unixcom}
Pour effacer tous les fichiers temporaires de \LaTeX{} :
\begin{unixcom}
  make cleantex
\end{unixcom}
Pour effacer tous les pdf (document maître et chapitres) :
\begin{unixcom}
  make cleandocs
\end{unixcom}


\section{Chapitrage}

Pour créer un fichier pdf en « 2 pages par page » par chapitre :
\begin{unixcom}
  ./makechaps.sh --2up
\end{unixcom}
L'option \verb+--2up+ est facultative.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../framabook"
%%% End:
