\section[Performances d'un algorithme]{%
  Estimer les performances d'un algorithme}
\index{complexité!d'un algorithme}

Pour le reste de ce chapitre nous allons présenter quelles sont les
techniques connues pour rendre les plus efficaces possible les
\emph{traitements élémentaires} et pour cela nous étudierons deux
grandes familles d'algorithmes : le \emph{tri} et la
\emph{recherche}. Si on y réfléchit quelques instants, on prendra
conscience du fait que de nombreux logiciels que nous utilisons
---~pour certains quotidiennement~--- passent beaucoup de temps à
trier ou rechercher des données. Depuis les années~60, les
mathématiciens, algorithmiciens, informaticiens se sont interrogés sur
la manière la plus efficace de trier des données ou de rechercher une
information parmi un ensemble. Les algorithmes mis au point sont
aujourd'hui implémentés dans les \sgbd{} si bien que lorsqu'on effectue
une requête de type tri ou recherche parmi les données d'une base, on
peut être certain que le traitement se fera dans un temps
optimal. Nous proposons ici d'exposer quels sont ces algorithmes et
comment on peut être sûr qu'ils répondent à la question efficacement
en termes de temps de calcul.

Le terme utilisé en algorithmie pour qualifier les performances d'un
algorithme, est \emph{complexité}, et plus précisément la complexité
en \emph{temps}. Il ne s'agit pas d'estimer le fait qu'un algorithme
soit complexe ou compliqué, mais d'estimer ses \emph{performances}.


\subsection{Notion d'opération fondamentale}
\index{complexité!opération fondamentale}

\begin{figure}[tb]
  \centering
  \includegraphics[width=.6\textwidth]{complexites}
  \caption[Différentes complexités]{Différentes
    complexités. \emph{Constante} : l'algorithme nécessitera le même
    temps quel que soit le nombre d'opérations fondamentales
    $N$. \emph{Linéaire} : le temps croît proportionnellement à $N$,
    \emph{quadratique} selon le carré de $N$, etc.}
  \label{fig-fondamentaux-complexites}
\end{figure}
Pour caractériser un algorithme, on détermine une ou plusieurs
\emph{opérations fondamentales} en fonction du nombre de données.  Par
exemple :
\begin{itemize}
\item pour des algorithmes de tris : les opérations fondamentales sont
  les opérations de comparaison  ;
\item pour des algorithmes de multiplication de matrices : ce sont la
  multiplication et l'addition ;
\item ...
\end{itemize}
On suppose donc, de manière implicite, que ce sont les opérations
fondamentales qui déterminent principalement le temps d'exécution des
algorithmes et que le temps d'exécution est fonction du nombre
d'opérations fondamentales (voir la
figure~\vref{fig-fondamentaux-complexites}). Cette approche ne permet
donc de comparer, entre-eux, que des algorithmes  travaillant sur le
même type de problème (on ne pourra comparer une méthode de tri avec
une méthode de multiplication de matrices).
\begin{nota}
  Il est important de comprendre ici que les courbes de complexité ne
  donnent pas une mesure du temps d'exécution, mais une idée de
  l'\emph{évolution de ce temps}. On pense parfois à tort que l'étude
  des complexités est inutile étant donné la puissance des ordinateurs
  d'aujourd'hui. Ceci est bien entendu une idée fausse. Même s'il est
  vrai qu'il y a de grandes disparités en termes de puissance de
  calcul entre différentes machines, car :
  \begin{itemize}
  \item on demande aux ordinateurs de traiter de plus en plus de
    données, par exemple : une image numérique d'un appareil photo
    datant de 1993 contenait $320\times200$ pixels, soit presque 200
    fois moins qu'un appareil d'aujourd'hui\footnote{Début du
      \textsc{xxi}\ieme{} siècle, vers 2018...} ;
  \item quelle que soit la puissance, une complexité quadratique
    implique que vouloir traiter $10$ fois plus de données demandera
    $10^2=100$ fois plus de temps.
  \end{itemize}
\end{nota}

Dans la suite de ce chapitre nous utiliserons les notations
suivantes\footnote{Cette notation n'est pas rigoureuse d'un point de
  vue mathématique, mais est utilisée ici sciemment par souci de
  clarté.} :
\begin{itemize}
  \index{complexité!constante}
\item $O(1)$ : pour les complexités en temps constant : quel que soit
  le nombre de données à traiter, on considérera que l'algorithme
  exigera le même temps ;
    \index{complexité!linéaire}
\item $O(N)$ : la complexité \emph{linéaire} : le temps augmente alors
  proportionnellement au nombre de données $N$ ;
    \index{complexité!quadratique}
\item $O(N^2)$ : la complexité \emph{quadratique} : on verra en
  particulier que dans le cadre du tri, cette complexité est
  considérée comme très mauvaise ;
\item $O(\log(N))$ : complexité \emph{logarithmique}. On verra plus
  loin qu'on parle ici du logarithme en base~2.
\end{itemize}
Il est parfois difficile de donner un ordre de grandeur du temps
nécessaire au déroulement d'un algorithme, dans tous les cas de
figure. C'est pourquoi on calcule ou on estime généralement :
\begin{itemize}
    \index{complexité!en moyenne}
\item la complexité \textbf{en moyenne}, c'est-à-dire le temps moyen que
  mettrait l'algorithme pour s'exécuter si on lui présentait des jeux
  de données correspondant à l'ensemble des configurations
  possibles.
    \index{complexité!pire des cas}
\item la complexité \textbf{dans le pire des cas}, c'est le temps mis
  par l'algorithme lorsqu'on lui présente un jeu de données le plus
  défavorable, c'est-à-dire le jeu de données pour lequel il doit
  effectuer le plus grand nombre d'opérations. 
%\item dans certains cas on peut également s'intéresser à la complexité
%  \textbf{dans le meilleur des cas} : le temps mis par l'algorithme
%  lorsqu'on lui présente un jeu de données le plus favorable. 
\end{itemize}


\subsection{Ordre d'idée de l'évolution des temps}

Pour avoir maintenant une idée de ce que représente une complexité
linéaire $O(N)$ par rapport à une complexité logarithmique
$O(\log_2(N))$, ou quadratique $O(N^2)$, suppposons : 
\begin{enumerate}
\item qu'on dispose d'un ordinateur capable d'effectuer 10 millions
  ($10^8$) d'opérations par seconde ;
\item que nous ayons un problème pour lequel le traitement de 1000
  données demande l'exécution de 100000 opérations, soit un
  millième de secondes.
\end{enumerate}

\subsubsection{Progression avec un algorithme linéaire}

Avec un algorithme linéaire, l'augmentation du nombre de données à
traiter entraînera une augmentation dans le même rapport, du temps de
calcul. Par exemple, en multipliant par 10 progressivement le nombre
de données :
\begin{center}
\begin{tabular}{||c|l|l||}
\hline
pour $10^3$ données& on a &$10^{-3}$ secondes de calcul\\
pour $10^4$ données& on aura &$10^{-2}$ secondes de calcul \\
pour $10^5$ données& on aura &$10^{-1}$ secondes de calcul\\
pour $10^6$ données& on aura &1 seconde de calcul\\
... & ... & ...\\
\hline
\end{tabular}
\end{center}
Finalement, pour $10^9$ (1 millards de) données, on aura 1000
secondes, soit environ un quart d'heure.\footnote{À la louche.}
%\item pour 10 milliards de données, au aura environ 200 minutes, soit
% environ 3h30\footnote{À la grosse louche.}


\subsubsection{Progression avec un algorithme quadratique}

Avec un algorithme quadratique le temps est augmenté
proportionnellement au carré du nombre de données. Donc multiplier
par~10 le volume de données à traiter conduit à un temps d'exécution
multiplié par~$10^2$ soit~100. Par conséquent la progression sera :

\begin{center}
  \begin{tabular}{||c|l|c||}
    \hline
   $10^3$ données&  $10^{-3}$ secondes& \\
   $10^4$ données&  $10^{-1}$ secondes  &\\
   $10^5$ données&  10 secondes  &\\
   $10^6$ données&  100 secondes  & environ 2
    minutes\\
   $10^7$ données &   200 minutes& soit environ 3h30\\
   $10^8$ données &  environ 350 heures&  c.-à-d. environ 15 jours\\
    \hline
  \end{tabular}
\end{center}
Finalement, pour le traitement d'un milliard de données il faudrait donc
1500 jours soit environ 4 ans.

\subsubsection{Progression avec un algorithme en $O(N\log N)$}

On verra que l'algorithme de référence pour le tri, le «~tri rapide~»
a une performance optimale\footnote{Vous ne trouverez pas d'algorithme
  de tri avec une complexité meilleure que celle-ci.} en~$O(N\log
N)$.
Pour ce type d'algorithme, à chaque fois que le volume de données sera
multiplié par 10, le temps de traitement sera multiplié par~$10\log10$
soit environ~$33,2$. Si on reprend notre progression :
\begin{center}
  \begin{tabular}{||c|c|c||}
    \hline
  $10^3$ données& $10^{-3}$ secondes  & \\
  $10^4$ données& $\frac{33}{1000}$ de secondes  &\\
  $10^5$ données& environ 1 seconde  &\\
  $10^6$ données& environ 30 secondes& \\
  $10^7$ données& environ 900 secondes & environ $\frac{1}{4}$ d'heure \\
  $10^8$ données& environ 33 quarts d'heure& environ 8 heures \\
    \hline
  \end{tabular}
\end{center}
Pour comparer avec les autres complexités, à 1 milliard de données le
temps de calcul atteint $33\times8$ heures, soit environ 11 jours.


\begin{nota}
  Ce qu'il faut retenir de tout ceci c'est que, entre un algorithme 
  de complexité quadratique et un algorithme de complexité $O(N\log N)$, un
  utilisateur :
  \begin{itemize}
  \item ne verra pas de différence notable entre attendre 33 millièmes
    de secondes ou attendre un dixième de seconde ;
  \item pourra éventuellement accepter d'attendre 2 minutes pour trier
    1 million d'éléments, mais appréciera peut-être d'attendre
    plutôt 30 secondes ;
  \item commencera sans doute à s'impatienter s'il faut attendre 3h30
    alors que le collègue/concurrent attend, lui, pas plus d'un quart
    d'heure ;
  \item lorsqu'il aura à lancer un calcul intensif, n'hésitera pas une
    seconde entre un programme exigeant un temps de traitement de 11 jours
    et un autre nécessitant 4 ans.
  \end{itemize}
\end{nota}




 \iffalse Cette supposition correspond aux
temps que nous avons trouvés au paragraphe précédent sur nos machines
test, puisque cela correspondrait à une machine capable d'effectuer
une opération en 10 nanosecondes. On notera~$\tau$ ce temps
élémentaire. Le tableau suivant indique pour différents volumes de
données quel est le temps estimé pour les complexités que nous
rencontrerons dans les chapitres suivants.
\begin{center}
  \begin{tabular}{|c|r|r|r|r|r|r|}
    \hline
%    \backslashbox{$N$}{Comp.} 
    $N$
    & $O(1)$ & $O(\log(N))$ & $O(N)$ & $O(N\log(N))$ & $O(N^2)$ &  $O(N^3)$ \\
    \hline
    $10^2$ & 10 ns & 66 ns & 1 $\mu$s & 7 $\mu$s & 100 $\mu$s & 10 ms \\
    $10^3$ & 10 ns & 100 ns & 10 $\mu$s & 100 $\mu$s & 10 ms & 10 s \\
    $10^4$ & 10 ns & 133 ns & 100 $\mu$s & 1 ms & 1 s & 3 h \\
    $10^5$ & 10 ns & 166 ns & 1 ms & 17 ms & 2 mn & 115.74 j \\
    $10^6$ & 10 ns & 199 ns & 10 ms & 199 ms & 3 h & 3.2 sc \\
    $10^7$ & 10 ns & 233 ns & 100 ms & 2 s & 11.57 j & $\infty$ \\
    $10^8$ & 10 ns & 266 ns & 1 s & 27 s & 3.2 ans & $\infty$ \\
    $10^9$ & 10 ns & 299 ns & 10 s & 5 mn & 3.2 sc & $\infty$ \\
    \hline
  \end{tabular}
\end{center}

Pour lire le tableau précédent, on pourra noter que :
\begin{itemize}
\item quel que soit le nombre de données $N$, on suppose que le temps
  nécessaire pour la complexité $O(1)$ est 10 nanosecondes,
  c'est-à-dire le temps nécessaire pour effectuer une opération sur
  notre machine ;
\item pour la complexité linéaire ($O(N)$) le temps est calculé en
  multipliant simplement le nombre de données par le temps élémentaire
  de 10 nanosecondes ;
\item pour les autres complexités, on procédera comme suit : par
  exemple pour $N=1000$ et la complexité quadratique $O(N^2)$ on
  considérera qu'il faut un temps de l'ordre de $1000^2 \times \tau$
  soit $1000000 \times 10^{-8}$ secondes, soit $10$ millisecondes. 
\end{itemize}
Il ressort de ce tableau, que des algorithmes dont la complexité est
quadratique ou plus mauvaise ($O(N^2)$ ou $O(N^3)$) sont à prohiber
pour des volumes de données supérieurs à~10000. On notera également
que pour un volume de données apparemment faible (par exemple 1000),
il y a un rapport de 100 entre le temps nécessaire pour exécuter un
algorithme de complexité $O(N^2)$ et un algorithme de complexité
$O(N\log_2 N)$. 
\fi


\endinput

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../framabook"
%%% End: 
