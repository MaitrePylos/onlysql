#!/bin/bash
# ------------------------------------------------------------------------
#
# Fabrique un fichier pdf par chapitre, en mode 2up
#   exige que le document maître (framabook.pdf) soit complètement compilé
#
# -------------------------------------------------------------------------

offset=8 # numéro de la première page à adapter si nécessaire

pdf="framabook.pdf"
opt=${1:-"xxx"}

for C in 1 2 3 4 5 6 7; do
    echo "Chapitre $C"
    pini=$[$(cat chapnum-$C.dat)+$offset]
    pfin=$[$(cat chapnum-$[C+1].dat)-1+offset]
    echo "   de page $pini à $pfin"
    if [ $opt != "--2up" ]; then
	pdftk  $pdf  cat $pini-$pfin output chap-$C.pdf
    else
        pdftk  $pdf  cat $pini-$pfin output - | pdftk blank-a5.pdf - output - | pdfnup --nup 2x1 --outfile chap-$C.pdf
    fi
done
