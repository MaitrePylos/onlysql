MAITRE=$(shell cat .maitre)

# chemins
FIGSDIR=figs
SVGSDIR=figs
EPSSDIR=epss
PDFSDIR=pdfs
IMGSDIR=pngs
TEXSDIR=texs
DATSDIR=dats


#----------------------------------------------------
#
# les fichiers de donn�es � tracer
#
#----------------------------------------------------
DATS2PDFS=$(PDFSDIR)/tri-insertion-1-CORE-I5.pdf $(PDFSDIR)/tri-insertion-2-CORE-I5.pdf  $(PDFSDIR)/tri-rapides-CORE-I5.pdf


#----------------------------------------------------
#
# les sources aux formats xfig
#
#----------------------------------------------------
FIGS=$(notdir $(wildcard $(FIGSDIR)/*.fig))
# ces sources converties en pdf
FIGS2PDFS=$(patsubst %,$(PDFSDIR)/%,$(FIGS:.fig=.pdf))

#----------------------------------------------------
#
# les sources aux formats svg
#
#----------------------------------------------------
SVGS=$(notdir $(wildcard $(SVGSDIR)/*.svg))
# ces sources converties en pdf
SVGS2PDFS=$(patsubst %,$(PDFSDIR)/%,$(SVGS:.svg=.pdf))


#-----------------------------------------------------
#
# Images
#
#-----------------------------------------------------
IMGS=$(notdir $(wildcard $(IMGSDIR)/*.png))
#-----------------------------------------------------

$(PDFSDIR)/%.pdf : $(DATSDIR)/%.dat
	graph -f 0.06 -g3 -X "N" -Y "temps (s)" -T ps  $< > temp.ps
	ps2epsi temp.ps temp.pdf
	epstopdf temp.pdf --outfile=$@
	rm -f temp.pdf

$(PDFSDIR)/%.pdf : $(FIGSDIR)/%.fig
ifndef mode
	fig2dev -L pdf  $< > $@
endif
ifeq ($(mode),nb)
	fig2dev -L pdf -N  $< > $@
endif
ifeq ($(mode),coul)
	fig2dev -L pdf   $< > $@
endif

$(PDFSDIR)/%.pdf : $(SVGSDIR)/%.svg
ifndef mode
	inkscape -A $@ $<
endif
ifeq ($(mode),coul)
	inkscape -A $@ $<
endif
ifeq ($(mode),nb)
	inkscape -A temp.pdf $<
	./pdf2gray.sh temp.pdf > $@
	rm -f temp.pdf
endif

$(PDFSDIR):
	mkdir -p $(PDFSDIR)


all: $(PDFSDIR)

figs: $(PDFSDIR) $(FIGS2PDFS) $(SVGS2PDFS) $(DATS2PDFS)

book:
	 pdftk  blank-a5.pdf $(MAITRE).pdf  output - |  pdfnup --nup 2x1 --outfile book.pdf

$(PDFSDIR)/tri-insertion-1-CORE-I5.pdf : $(DATSDIR)/tri-insertion-1-CORE-I5.dat $(DATSDIR)/tri-insertion-presque-1-CORE-I5.dat
	graph -f 0.06 -g3 -X "N" -Y "temps (s)" -T ps -m1 $(word 1,$^) -m2 $(word 2, $^) > temp.ps
	ps2epsi temp.ps temp.pdf
	epstopdf temp.pdf --outfile=$@
	rm -f temp.ps

$(PDFSDIR)/tri-insertion-2-CORE-I5.pdf : $(DATSDIR)/tri-insertion-2-CORE-I5.dat $(DATSDIR)/tri-insertion-presque-2-CORE-I5.dat
	graph -f 0.06 -g3 -X "N" -Y "temps (s)" -T ps -m0 $(word 1,$^) -m1 $(word 2, $^) > temp.ps
	ps2epsi temp.ps temp.pdf
	epstopdf temp.pdf --outfile=$@
	rm -f temp.ps temp.pdf

$(PDFSDIR)/tri-rapides-CORE-I5.pdf : $(DATSDIR)/tri-shell-CORE-I5.dat $(DATSDIR)/tri-rapide-CORE-I5.dat
	graph -x 0 100000000 -f 0.06 -g3 -X "N" -Y "temps (s)" -T ps -m0 $(word 1,$^) -m1 $(word 2, $^) > temp.ps
	ps2epsi temp.ps temp.pdf
	epstopdf temp.pdf --outfile=$@
	rm -f temp.ps temp.pdf 

cleandocs:
	rm -f book.pdf frama*.pdf chap-*.pdf *.tmp 

cleanfigs:
	rm -f $(FIGS2PDFS) 
	rm -f $(SVGS2PDFS) 
	rm -f $(IMGSDIR)/page-*.png
	rm -rf $(DATS2PDFS)

cleantex :
	rm -f chapnum*.dat
	rm -f $(MAITRE)~     $(MAITRE).bak
	rm -f $(MAITRE).aux  $(MAITRE).log $(MAITRE).blg $(MAITRE).bbl 
	rm -f $(MAITRE).exa  $(MAITRE).bmt $(MAITRE).out $(MAITRE).som
	rm -f $(MAITRE).ind  $(MAITRE).idx $(MAITRE).ilg $(MAITRE).adx 
	rm -f $(MAITRE).mtc* $(MAITRE).toc $(MAITRE).lof 
	rm -f $(MAITRE).glo  $(MAITRE).glx $(MAITRE).tns $(MAITRE).tpt 
	rm -f $(MAITRE).maf  $(MAITRE).brf
	rm -f $(MAITRE).ilgg
	rm -f *.aux

index :
	makeindex -s bibidx/manuel.ist $(MAITRE).idx
	makeindex -s bibidx/glossaire.ist $(MAITRE).glo -o $(MAITRE).glx -t $(MAITRE).ilgg
bib:
	BSTINPUTS=bibidx/: bibtex $(MAITRE)

