Production du document
======================

Prérequis
---------

* Une distrib TeX, les paquets debian suivants devraient suffire :
  * texlive-base
  * texlive-binaries
  * texlive-extra-utils
  * texlive-font-utils
  * texlive-fonts-extra
  * texlive-fonts-recommended
  * texlive-generic-recommended
  * texlive-lang-french 
  * texlive-latex-base
  * texlive-latex-extra
  * texlive-latex-recommended
  * texlive-pictures
  * texlive-science
* plotutils
* transfig (fait partie de xfig)
* inkscape
* gs (ghostscript)
* ps2epsi (ghostscript)
* epstopdf (doit faire partie de la distrib TeX, sinon https://ctan.org/pkg/epstopdf)
* pdfnup (idem, https://ctan.org/pkg/pdfjam)

Compilation
-----------

1. Choisir la version à compiler, pour cela dans le document maître
``framabook.tex``, choisir l'une des deux options de documents :
  * soit ``versionenligne`` qui produit un pdf uniquement destiné à être lu
  * soit ``versionpapier`` pour un pdf destiné à être imprimé
2. Choisir la version couleur ou passer en noir & blanc, dans ce cas ajouter ``nb`` dans les options de classe
3. Créer les figures :
  * soit ``make mode=nb figs``
  * soit ``make figs``
4. LaTeX Passe 1 : ``pdflatex framabook``
5. Création de la biblio et de l'index
  * ``make bib``
  * ``make index``
6. LaTeX passe 2 : ``pdflatex framabook``
7. LaTeX passe 3 : ``pdflatex framabook``

Imprimer
--------

* ``./makedistrib.sh`` fabrique toutes les version des fichiers ``onlysql-xxx.pdf`` 
* ``make book`` fabrique le fichier book.pdf qui pourra être imprimé sur du A4 avec un reliure longue


Nettoyage
---------
* ``make cleanfigs`` détruit toutes les figures générées en pdf à partir des sources xfig/svg
* ``make cleantex`` efface tous les fichiers temporaires de LaTeX
* ``make cleandocs`` efface tous les pdf 

Chapitrage
----------

* ``./makechaps.sh`` crée un fichier pdf par chapitre
* ``./makechaps.sh --2up`` crée un fichier pdf en « 2up » par chapitre

